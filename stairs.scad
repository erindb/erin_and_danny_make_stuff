t = 1.5;
f = 3.5;
p = 0.453;
buffer = 10;

height = 60.5 + p;
longest_possible_screw = 2.75;
short_screw = 1.75;
top_platform_width = 16.5;
top_platform_depth = 18 + p;
step_height = 9;
step_depth = 4.5;
function steps_height(n) = step_height * n;

module hamper() {
    translate([1, 2, 36]) {
        cube([16.25, 12.125, 22.25]);
    }
}

module litterbox() {
    translate([p, 41 - 15 - 4, 0]) {cube([19.25, 15, 5]);}
}

module food() {
    translate([18/2 + p, 16.5/2, 18]) {cylinder(r = 15/2, h = 13.5);}
}

module screw(length = longest_possible_screw){
    shortest_possible_length = short_screw;
    color_component = (length - shortest_possible_length)
        / (longest_possible_screw - shortest_possible_length);
    c = [color_component, color_component, color_component];
    color(c){
        cylinder( r = 0.1, h = length);}}

module angle_bracket(x = 0, y = 0, z = 0, rotation = [0, 0, 0], screw_length = longest_possible_screw, distance_from_center = 2) {
    translate([x, y, z]){
        rotate(rotation){
            translate([-distance_from_center, 0, 0]){
                screw(screw_length);}
            translate([0, 0, -distance_from_center]){
                rotate([0, 90, 0]){
                    screw(screw_length);}}}}}

module stairs() {
    // screws -- top frame to legs
    translate([p + f/4, t / 2, 57.5]) {
        screw();
    }
    translate([p + f * 3 / 4, t / 2, 57.5]) {
        screw();
    }
    translate([p + f/4, 16.5 - t / 2, 57.5]) {
        screw();
    }
    translate([p + f * 3 / 4, 16.5 - t / 2, 57.5]) {
        screw();
    }

    //TODO: redo these as 1 inch in from the side,
    // rather than 1/4 and 3/4 of f
    translate([p + 18 - f/4, t / 2, 57.5]) {
        screw();
    }
    translate([p + 18 - f * 3 / 4, t / 2, 57.5]) {
        screw();
    }
    translate([p + 18 - f/4, 16.5 - t / 2, 57.5]) {
        screw(); 
    }
    translate([p + 18 - f * 3 / 4, 16.5 - t / 2, 57.5]) {
        screw();
    }
    angle_bracket(x = 4, y = t/2, z = 57, rotation = [0, -90, 0]);
    angle_bracket(x = 18 + p - f, y = t/2, z = 57, rotation = [0, 0, 0]);
    angle_bracket(x = 4, y = 16.5 - t/2, z = 57, rotation = [0, -90, 0]);
    angle_bracket(x = 18 + p - f, y = 16.5 - t/2, z = 57, rotation = [0, 0, 0]);
    
    // screws -- top platform to top frame
    translate([f/2, 16.5/4, height - short_screw]) {screw(short_screw);}
    translate([f/2, 16.5*3/4, height - short_screw]) {screw(short_screw);}
    translate([18 - f/2 + p, 16.5/4, height - short_screw]) {screw(short_screw);}
    translate([18 - f/2 +p, 16.5*3/4, height - short_screw]) {screw(short_screw);}
    translate([18/2 + p, t/2, height - short_screw]) {screw(short_screw);}
    translate([18/2 + p, 16.5 - t/2, height - short_screw]) {screw(short_screw);}

    // screws -- back to frame
    translate([0, 16.5/2, 60.5 - t/2]) {
        rotate([0, 90, 0]) {
            screw();
        }
    }
    
    // screws for step 5 (ground is 0)
    translate([p + f / 2, top_platform_width - t, steps_height(5) - p / 2]){
        rotate([-90, 0, 0]){
            screw();}}
    translate([top_platform_depth - f / 2, top_platform_width - t, steps_height(5) - p / 2]){
        rotate([-90, 0, 0]){
            screw();}}
            
    // screws for bookshelf backing to tall legs
    translate([p + f - 1, top_platform_width + p, steps_height(5) - step_height / 4]){
        rotate([90, 0, 0]){
            screw(short_screw);}}
    translate([p + f - 1, top_platform_width + p, steps_height(4) + step_height / 4]){
        rotate([90, 0, 0]){
            screw(short_screw);}}    
    translate([top_platform_depth - (f - 1), top_platform_width + p, steps_height(5) - step_height / 4]){
        rotate([90, 0, 0]){
            screw(short_screw);}}
    translate([top_platform_depth - (f - 1), top_platform_width + p, steps_height(4) + step_height / 4]){
        rotate([90, 0, 0]){
            screw(short_screw);}}

    // screws for step 5 top to step 5 side supports (going down)
    translate([p + t / 2, top_platform_width + step_depth - 1, steps_height(5)]){
        rotate([180, 0, 0]){
            screw(short_screw);}}
    translate([p + t / 2, top_platform_width + step_depth - (f - 1), steps_height(5)]){
        rotate([180, 0, 0]){
            screw(short_screw);}}
    translate([top_platform_depth - t / 2, top_platform_width + step_depth - 1, steps_height(5)]){
        rotate([180, 0, 0]){
            screw(short_screw);}}
    translate([top_platform_depth - t / 2, top_platform_width + step_depth - (f - 1), steps_height(5)]){
        rotate([180, 0, 0]){
            screw(short_screw);}}
    
    // screws for step 4 top to step 5 side supports (going up)
    translate([p + t / 2, top_platform_width + step_depth - 1, steps_height(4)-p]){
        rotate([0, 0, 0]){
            screw(short_screw);}}
    translate([p + t / 2, top_platform_width + step_depth - (f - 1), steps_height(4)-p]){
        rotate([0, 0, 0]){
            screw(short_screw);}}
    translate([top_platform_depth - t / 2, top_platform_width + step_depth - 1, steps_height(4)-p]){
        rotate([0, 0, 0]){
            screw(short_screw);}}
    translate([top_platform_depth - t / 2, top_platform_width + step_depth - (f - 1), steps_height(4)-p]){
        rotate([0, 0, 0]){
            screw(short_screw);}}
            
    // screws for  step 4 top to step 4 side supports
    translate([p + t / 2, top_platform_width + step_depth*2 - 1, steps_height(4)]){
        rotate([180, 0, 0]){
            screw(short_screw);}}
    translate([p + t / 2, top_platform_width + step_depth*2 - (f - 1), steps_height(4)]){
        rotate([180, 0, 0]){
            screw(short_screw);}}
            // diagonals
    translate([top_platform_depth - 1, top_platform_width + step_depth*2 - 1.75, steps_height(4)]){
        rotate([180, 0, 0]){
            screw(short_screw);}}
    translate([top_platform_depth - (f-1), top_platform_width + step_depth*2 - 1.75, steps_height(4)]){
        rotate([180, 0, 0]){
            screw(short_screw);}}
            
    // screws for step 4 into 2x2 support beams
    translate([p + f + 1, t + t/2, steps_height(4)]) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    translate([top_platform_depth - (f + 1), t + t/2, steps_height(4)]) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    translate(
        [p + f + 1,
         top_platform_width - (t + t/2),
         steps_height(4)]
    ) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    translate(
        [top_platform_depth - (f + 1),
         top_platform_width - (t + t/2),
         steps_height(4)]
    ) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    
    // screws for step 4 2x2 support beams into 2x4 legs
    translate([
        p + (f- 1),
        t + t,
        steps_height(4) - p - t/2
    ]) {rotate([90, 0, 0]) {screw();}}
    translate([
        p + 1,
        t + t,
        steps_height(4) - p - t/2
    ]) {rotate([90, 0, 0]) {screw();}}
    translate([
        top_platform_depth - 1,
        t + t,
        steps_height(4) - p - t/2
    ]) {rotate([90, 0, 0]) {screw();}}
    translate([
        top_platform_depth - (f - 1),
        t + t,
        steps_height(4) - p - t/2
    ]) {rotate([90, 0, 0]) {screw();}}
    translate([
        p + 1,
        top_platform_width - (t + t),
        steps_height(4) - p - t/2
    ]) {rotate([-90, 0, 0]) {screw();}}
    translate([
        p + (f-1),
        top_platform_width - (t + t),
        steps_height(4) - p - t/2
    ]) {rotate([-90, 0, 0]) {screw();}}
    translate([
        top_platform_depth - 1,
        top_platform_width - (t + t),
        steps_height(4) - p - t/2
    ]) {rotate([-90, 0, 0]) {screw();}}
    translate([
        top_platform_depth - (f - 1),
        top_platform_width - (t + t),
        steps_height(4) - p - t/2
    ]) {rotate([-90, 0, 0]) {screw();}}
    
    // screws for step 4 side support to step 3
    // ORDER_CONSTRAINT: screw this before attaching step 3 to lower supports
    translate([p + t / 2, top_platform_width + step_depth*2 - 1, steps_height(3)-p]){
        rotate([0, 0, 0]){
            screw(short_screw);}}
    translate([p + t / 2, top_platform_width + step_depth*2 - (f - 1), steps_height(3)-p]){
        rotate([0, 0, 0]){
            screw(short_screw);}}
            // diagonals
    translate([
            top_platform_depth - 1,
            top_platform_width - t,
            steps_height(3) + 1.5]
         ){
        rotate([-90, 0, 0]){
            screw();}}
    translate([
            top_platform_depth - (f-1),
            top_platform_width - t,
            steps_height(3) + 1.5]
         ){
        rotate([-90, 0, 0]){
            screw();}}

    // screws for  step 3 into supports
    translate([
            p + t/2,
            top_platform_width + 4,
            steps_height(3)
        ]) {
        rotate([180, 0, 0]) {screw();}
    }
    translate([
            p + t/2,
           41 - 4,
            steps_height(3)
        ]) {
        rotate([180, 0, 0]) {screw();}
    }
    translate([
           p + f/2,
           41 - t/2,
            steps_height(3)
        ]) {
        rotate([180, 0, 0]) {screw();}
    }
    translate([
           12 - 1,
           41 - t/2,
            steps_height(3)
        ]) {
        rotate([180, 0, 0]) {screw();}
    }
    translate([
           12 - (f - 1),
           41 - t/2,
            steps_height(3)
        ]) {
        rotate([180, 0, 0]) {screw();}
    }
    
    // screws for legs to support for step 3
    translate([
            p + t/2,
            top_platform_width - t,
            steps_height(3) - p - 1
        ]) {
        rotate([-90, 0, 0]) {screw();}
    }
    translate([
            p + t/2,
            top_platform_width - t,
            steps_height(3) - p - (f - 1)
        ]) {
        rotate([-90, 0, 0]) {screw();}
    }
    translate([
            p + t/2,
            41,
            steps_height(3) - p - 1
        ]) {
        rotate([90, 0, 0]) {screw();}
    }
    translate([
            p + t/2,
            41,
            steps_height(3) - p - (f - 1)
        ]) {
        rotate([90, 0, 0]) {screw();}
    }
    translate([
            p + t + (f - t)/2,
            top_platform_width - t,
            steps_height(3) - p - t/2
        ]) {
        rotate([-90, 0, 0]) {screw();}
    }
    
    // screws for back panel to support for step 4
    translate([
        0,
        top_platform_width + step_depth*2 - f/2,
        steps_height(4) - (steps_height(4) - steps_height(3))*0.25
    ]) {
        rotate([0, 90, 0]) {screw(short_screw);}
    }
    translate([
        0,
        top_platform_width + step_depth*2 - f/2,
        steps_height(4) - (steps_height(4) - steps_height(3))*0.75
    ]) {
        rotate([0, 90, 0]) {screw(short_screw);}
    }
    
    // screws for back panel to support for step 5
    translate([
        0,
        top_platform_width + step_depth - f/2,
        steps_height(5) - (steps_height(5) - steps_height(4))*0.25
    ]) {
        rotate([0, 90, 0]) {screw(short_screw);}
    }
    translate([
        0,
        top_platform_width + step_depth - f/2,
        steps_height(5) - (steps_height(5) - steps_height(4))*0.75
    ]) {
        rotate([0, 90, 0]) {screw(short_screw);}
    }
    
    // screws for back panel to support for step 3
    translate([0, top_platform_width + (41 - top_platform_width)*.75, steps_height(3) - p - f/2]) {
        rotate([0, 90, 0]) {screw(short_screw);}}
    translate([0, top_platform_width + (41 - top_platform_width)*.25, steps_height(3) - p - f/2]) {
        rotate([0, 90, 0]) {screw(short_screw);}}
        
    // screws for step 2 into 2x2 support beams
    translate([p + f + 1, t + t/2, steps_height(2)]) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    translate([top_platform_depth - (f + 1), t + t/2, steps_height(2)]) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    translate(
        [p + f + 1,
         top_platform_width - (t + t/2),
         steps_height(2)]
    ) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    translate(
        [top_platform_depth - (f + 1),
         top_platform_width - (t + t/2),
         steps_height(2)]
    ) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    
    // screws for step 2 into 2x4 support beam
    translate([
            12 + step_depth - t/2,
            top_platform_width + 4,
            steps_height(2)
        ]) {
        rotate([180, 0, 0]) {screw();}
    }
    translate([
            12 + step_depth - t/2,
           41 - 4,
            steps_height(2)
        ]) {
        rotate([180, 0, 0]) {screw();}
    }
    
    // screws for step 2 2x2 support beams into 2x4 legs
    translate([
        p + 1,
        t + t,
        steps_height(2) - p - t/2
    ]) {rotate([90, 0, 0]) {screw();}}
    translate([
        top_platform_depth - 1,
        t + t,
        steps_height(2) - p - t/2
    ]) {rotate([90, 0, 0]) {screw();}}
    translate([
        p + 1,
        top_platform_width - (t + t),
        steps_height(2) - p - t/2
    ]) {rotate([-90, 0, 0]) {screw();}}
    translate([
        top_platform_depth - 1,
        top_platform_width - (t + t),
        steps_height(2) - p - t/2
    ]) {rotate([-90, 0, 0]) {screw();}}
    translate([
        p + (f-1),
        t + t,
        steps_height(2) - p - t/2
    ]) {rotate([90, 0, 0]) {screw();}}
    translate([
        top_platform_depth - (f-1),
        t + t,
        steps_height(2) - p - t/2
    ]) {rotate([90, 0, 0]) {screw();}}
    translate([
        p + (f-1),
        top_platform_width - (t + t),
        steps_height(2) - p - t/2
    ]) {rotate([-90, 0, 0]) {screw();}}
    translate([
        top_platform_depth - (f-1),
        top_platform_width - (t + t),
        steps_height(2) - p - t/2
    ]) {rotate([-90, 0, 0]) {screw();}}
    
    
    // screws for legs to support for step 2
    translate([
            12 + step_depth - t/2,
            top_platform_width - t,
            steps_height(2) - p - 1
        ]) {
        rotate([-90, 0, 0]) {screw();}
    }
    translate([
            12 + step_depth - t/2,
            top_platform_width - t,
            steps_height(2) - p - (f - 1)
        ]) {
        rotate([-90, 0, 0]) {screw();}
    }
    translate([
            12 + step_depth - t/2,
            41,
            steps_height(2) - p - 1
        ]) {
        rotate([90, 0, 0]) {screw();}
    }
    translate([
            12 + step_depth - t/2,
            41,
            steps_height(2) - p - (f - 1)
        ]) {
        rotate([90, 0, 0]) {screw();}
    }
    translate([
            12 - (f - 1),
            41,
            steps_height(2) - p - t/2
        ]) {
        rotate([90, 0, 0]) {screw();}
    }
    translate([
            12 - 1,
            41,
            steps_height(2) - p - t/2
        ]) {
        rotate([90, 0, 0]) {screw();}
    }
    translate([
            12  + step_depth - (f-1),
            41,
            steps_height(2) - p - t/2
        ]) {
        rotate([90, 0, 0]) {screw();}
    }
    
    // screws for legs to support for step 1
    translate([
            top_platform_depth - 1,
            top_platform_width + t,
            steps_height(1) - p - t/2
        ]) {
        rotate([90, 0, 0]) {screw();}
    }
    translate([
            top_platform_depth - (f-1),
            top_platform_width + t,
            steps_height(1) - p - t/2
        ]) {
        rotate([90, 0, 0]) {screw();}
    }
    // screws for down through step 1 into supports
    translate([
            12 + 4.5 + 4.5 - 1,
            top_platform_width + t / 2,
            steps_height(1)
        ]) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    translate([
            12 + 4.5 + 4.5 - 1,
            41 - t / 2,
           steps_height(1)
        ]) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    translate([
            12 + 4.5 + 4.5 - step_depth  + 1,
            41 - t / 2,
            steps_height(1)
        ]) {
        rotate([180, 0, 0]) {screw(short_screw);}
    }
    
    // screws for short side wall into support legs, going towards origin
    translate([p + f / 2, 41 + p, 3]){
        rotate([90, 0, 0]){
            screw(short_screw);}}
    translate([p + f / 2, 41 + p, steps_height(1) - 2]){
        rotate([90, 0, 0]){
            screw(short_screw);}}

    translate([p + 11.5 - f / 2, 41 + p, 3]){
        rotate([90, 0, 0]){
            screw(short_screw);}}
    translate([p + 11.5 - f / 2, 41 + p, steps_height(1) - 2]){
        rotate([90, 0, 0]){
            screw(short_screw);}}

    translate([p + 11.5 + 4.5 - f / 2, 41 + p, 3]){
        rotate([90, 0, 0]){
            screw(short_screw);}}
    translate([p + 11.5 + 4.5 - f / 2, 41 + p, steps_height(1) - 2]){
        rotate([90, 0, 0]){
            screw(short_screw);}}

    translate([p + 11.5 + 4.5 + f - f / 2, 41 + p, 3]){
        rotate([90, 0, 0]){
            screw(short_screw);}}
    translate([p + 11.5 + 4.5 + f - f / 2, 41 + p, steps_height(1) - 2]){
        rotate([90, 0, 0]){
            screw(short_screw);}}
            
            
    // screws for up from the bottom through the bottom board into the support legs
    translate([p + 1, t / 2, -p]){
            screw();}
    translate([p + f - 1, t / 2, -p]){
            screw();}

    translate([top_platform_depth - f + 1, t / 2, -p]){
            screw();}
    translate([top_platform_depth - 1, t / 2, -p]){
            screw();}


    translate([p + 1, top_platform_width - t / 2, -p]){
            screw();}
    translate([p + f - 1, top_platform_width - t / 2, -p]){
            screw();}

    translate([top_platform_depth - f + 1, top_platform_width - t / 2, -p]){
            screw();}
    translate([top_platform_depth - 1, top_platform_width - t / 2, -p]){
            screw();}



    translate([p + 1, 41 - t / 2, -p]){
            screw();}
    translate([p + f - 1, 41 - t / 2, -p]){
            screw();}

    translate([12 - 1, 41 - t / 2, -p]){
            screw();}
    translate([12 - f + 1, 41 - t / 2, -p]){
            screw();}


    translate([12 + 4.5 - 1, 41 - t / 2, -p]){
            screw();}
    translate([12 + 4.5 - f + 1, 41 - t / 2, -p]){
            screw();}


    translate([12 + f + 4.5 - 1, 41 - t / 2, -p]){
            screw();}
    translate([12 + f + 4.5 - f + 1, 41 - t / 2, -p]){
            screw();}

    // legs
    color([1.0, 1.0, 0, 0.1]) {
        translate([p, 0, 0]) {cube([f, t, 59]);}
        translate([15, 0, 0]) {cube([f, t, 59]);}
        translate([p, 15, 0]) {cube([f, t, 59]);}
        translate([15, 15, 0]) {cube([f, t, 59]);}
        translate([p, 41 - t, 0]) {cube([f, t, 9*3 - p]);}
        translate([12 - 3.5, 41 - t, 0]) {cube([f, t, 9*3 - p]);}
        translate([12 + 1, 41 - t, 0]) {cube([f, t, 9*2 - p]);}
        translate([12 + 4.5, 41 - t, 0]) {cube([f, t, 9 - p]);}
    }
    
    // support beams
    color([1.0, 0, 1.0, 0.1]) {
        translate([p, 0, 59]) {cube([f, 16.5, t]);}
    }
    color([1.0, 0, 1.0, 0.1]) {
        translate([15, 0, 59]) {cube([f, 16.5, t]);}
    }
    color([1.0, 0, 1.0, 0.1]) {
        translate([4, 0, 57]) {cube([11, t, f]);}
    }
    color([1.0, 1.0, 0, 0.1]) {
        translate([4, 15, 57]) {cube([11, t, f]);}
    }
    
    color([1.0, 1.0, 0, 0.1]) {
        translate([15, 16.5, 2*9 - p - f]) {
            cube([t, 41 - 16.5 - t, f]);
        }
    }
    color([1.0, 1.0, 0, 0.1]) {
        translate([p, 16.5, 3*9 - p - f]) {
            cube([t, 23, f]);
        }
    }
    
    // support blocks
    color([1.0, 0, 0.5, 0.1]) {
        translate([15, 16.5, 9 - p - t]) {
            cube([3.5 + 2.5, t, t]);
        }
    }
    color([1.0, 0, 0.5, 0.1]) {
        translate([p, 16.5 - t - t, 2*9 - p - t]) {
            cube([18, t, t]);
        }
    }
    color([1.0, 0, 0.5, 0.1]) {
        translate([p, t, 2*9 - p - t]) {cube([18, t, t]);}
    }
    color([1.0, 0, 0.5, 0.1]) {
        translate([12 - 3.5, 41 - t - t, 2*9 - p - t]) {
            cube([4.5 + 3.5 - t, t, t]);
        }
    }
    color([1.0, 0, 0.5, 0.1]) {
        translate([p + t, 16.5, 3*9 - p - t]) {
            cube([f - t, t, t]);
        }
    } 
    color([1.0, 0, 0.5, 0.1]) {
        translate([p, t, 9 * 4 - p - t]){
            cube([18, t, t]);}}
    color([1.0, 0, 0.5, 0.1]) {
        translate([p, 16.5 - t - t, 9 * 4 - p - t]){
            cube([18, t, t]);}}

    // diagonal
    color([1.0, 1.0, 0, 0.1]) {
        difference() {
            translate([18 - f + p, 16.5, 27 - p]){
                rotate([45, 0, 0]){
                    cube([f, 9 * sqrt(2), t]);}}
            translate([0, 0, 9*4 - p]) {cube([1000, 1000, 1000]);}
        }
    }
            
    color([1.0, 1.0, 0, 0.1]) {
        translate([p, 16.5 + 2*4.5 - f, 3*9]){
            cube([t, f, 9 - p]);}}
        
    color([1.0, 1.0, 0, 0.1]) {
        translate([p, 16.5 + 4.5 - f, 4*9]){
            cube([t, f, 9 - p]);
        }
    }
        
    // back of bookshelf
    // FLAT --- 18 x 8.5
    color([0, 0.8, 1.0, .1]) {
        translate([p, 16.5, 4*9]){
            cube([18, p, 9 - p]);
        }
    }
        
    color([1.0, 1.0, 0, 0.1]) {
        translate([18 + p - t, 16.5 + 4.5 - f, 4*9]){
            cube([t, f, 9 - p]);}
    }
    
    // platforms
    // FLAT --- 18.5 x 16.5
    color([0, .8, 1.0]) {
    translate([0, 0, 59 + t]) {cube([18 + p, 16.5, p]);}}
    
    // FLAT --- 18 x 4.5
    color([0, .8, 1.0, .1]) {
        translate([p, 16.5, 5 * 9 - p]) {
            cube([18, 4.5, p]);
        }
    }
    
    // FLAT --- 18 x 25.5
    color([0, .8, 1.0]) {
    translate([p, 0, 4*9 - p]){
        cube([18, 16.5 + 4.5 + 4.5, p]);}}
    
    
    // FLAT --- complicated
    color([0, .8, 1.0]) {
    difference(){
        translate([p, 16.5, 3*9 - p]) {
            cube([12-p, 41 - 16.5, p]);}
        translate([4 + 0.1 +p, 16.5 - 0.1, 0]){
            linear_extrude(height = 100){
                polygon([[0-p, 0],
                         [8-p, 0],
                         [8, 24.5]]);}}}}
    
    // FLAT --- complicated
    color([0, .8, 1.0]) {
        union() {
            translate([p, 0, 2*9 - p]){
                cube([18, 16.5, p]);}
            translate([12, 16.5, 2*9 - p]){
                cube([4.5, 41 - 16.5, p]);}
            translate([12 - 8+p, 16.5, 2*9 - p]){
                linear_extrude(height = p){
                    polygon([[0-p, 0],
                             [8-p, 0],
                             [8-p, 10]]);}}}}
    
    // FLAT --- 4.5 X 24.5
    color([0, .8, 1.0]) {
        translate([12 + 4.5, 16.5, 9 - p]){
            cube([4.5, 41 - 16.5, p]);}
    }

    // back wall
    // FLAT --- 16.5 x 60.5
    color([0, .8, 1.0, 0.1]) {
        translate([0, 0, 0]) {cube([p, 16.5, 60.5]);}
    }
    // FLAT --- 24.5 x 45
    color([0, .8, 1.0, .1]) {
        translate([0, 16.5, 0]) {cube([p, 41 - 16.5, 9*5]);}
    }
    
    // side wall
    // FLAT --- 21 x 9
    color([0, .8, 1.0, .1]) {
    translate([0, 41, 0]) {cube([12 + 4.5 + 4.5, p, 9]);}
    }
    
    // floor
    // FLAT --- 20.5 x 41
    color([0, 0.8, 1.0, .1]) {
        translate([p, 0, -p]) {
            cube([20.5, 41, p]);
        }
    }
}

module white_dresser() {
    translate([0, 42, 0]) {cube([17, 43, 34]);}
}

module minecraft_zoe(){
    /*
    12 shoulder to butt
    18 tall when pooping
    10 floor to shoulder
    5 shoulder to shoulder
    10 rear haunches
    */
    color([0.2, 0.2, 0.2]){
    translate([0, 0, 0]){cube([1, 1, 5]);}
    translate([0, 12 - 1, 0]){cube([1, 1, 5]);}
    translate([5 - 1, 0, 0]){cube([1, 1, 5]);}
    translate([5 - 1, 12 - 1, 0]){cube([1, 1, 5]);}
    translate([0, 0, 5]){cube([5, 12, 5]);}
    translate([0, -2.5, 10]){cube(5);}}
}

module room() {
//translate([10, 20, 9*2]) { minecraft_zoe(); }
//intersection(){
//    translate([0, 22, 10]){cube(20);}
//union(){
//%white_dresser();
//%food();
stairs();
//%hamper();
//%litterbox();
//}}
}

/*
what to do:
[x] 0. prep
  0.0 clear area for cutting in garage
[x] 1. buy supplies
  1.-1 find out what supplies we have already,
       and how much we need total,
       and calculate how much to buy
  1.0 buy 4 of angle brackets
  1.1 buy 74 of screws that are 2.75" long; buy 49 of screws that are 1.75" long
  1.2 buy 1 of 8' length of 2x2
  1.3 buy 5 of 8' length 2x4
  1.4 4'x8'x0.5 2 sheets
[x] 2. cut supplies
  2.0 measure
  2.1 cut 2x2s
  2.2 cut 2x4s
  2.3 cut platforms
[x] 3. sand things and paint? primer?
4. put supplies back together
5. put carpet?
*/